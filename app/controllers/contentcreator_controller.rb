class ContentcreatorController < ApplicationController
  def loadingbar
    ludumstarttime = Time.utc(2013, 8, 24, 1, 0, 0);
    loadingstarttime = Time.utc(2013, 8, 23, 1, 0, 0);
    timenow = Time.now
	if timenow > ludumstarttime then
		render File.read("./content/loaded.svg")
	else
		data = File.read("./content/werein.svg")
		lines = data.each_line("\n").to_a
		t = ludumstarttime - timenow
		out = Array.new
		lines[1133].gsub!(/dur=".*?"/, "dur=\"" + t.to_s + "s\"")
		lines[1133].gsub!(/from=".*?"/, "from=\"" + ((timenow- loadingstarttime) / 86400 * 831.7).to_s + "\"")
		i=0
		for i in 0..9
		  lines[873+i*16].gsub!(/begin=".*?"/, "begin=\"" + (t % 10 - (i + 1)*1).to_s + "s\"")
		  lines[564+i*16].gsub!(/begin=".*?"/, "begin=\"" + (t % 600 - (i + 1) *60).to_s + "s\"")
		  lines[265+i*16].gsub!(/begin=".*?"/, "begin=\"" + (t % 36000 - (i + 1) *3600).to_s + "s\"")
		  out.push lines[265 + i * 16]
		end
		for i in 0..6
		  lines[762+i*16].gsub!(/begin=".*?"/, "begin=\"" + (t % 60 - (i + 1)*10).to_s + "s\"")
		  lines[453+i*16].gsub!(/begin=".*?"/, "begin=\"" + (t % 3600 - (i + 1) *600).floor.to_s + "s\"")
		end
		lines[193] = "begin=\"" + (t - 36000).to_s + "s\""
		lines[208] = "dur=\"" + (t - 36000).to_s + "s\""
		if t - 36000 < 0 then
		  for i in 196 .. 211
			lines[i] = ""
		  end
		end
		render :text => lines.join, :content_type => "image/svg+xml"
	end
    #render :text => out.join("<br/>")
    #send_file './content/werein.svg', :x_sendfile => true, :type => 'image/svg+xml', :disposition => 'inline'
  end
end
